

#include <iostream>

class Spells
{
public:
 
    void PrintAllSpells();
    
    void SpellOfFirstImage();
    void SpellOfSecondImage();
    void SpellOfThirdImage();
};

void Spells::PrintAllSpells()
{
    std::cout<< "First image" << std::endl;
    SpellOfFirstImage();
    
    std::cout<< "Second image" << std::endl;
    SpellOfSecondImage();
    
    std::cout<< "Third image" << std::endl;
    SpellOfThirdImage();
    
}

void Spells::SpellOfFirstImage()
{
    for(int i = 0; i < 25; i++)
    {
        for(int j = 0; j < 25;  j++)
            i < j ? std::cout << "#" : std::cout << ".";
        std::cout<< std::endl;
    }   
}

void Spells::SpellOfSecondImage()
{
    for(int i = 0; i < 25; i++)
    {
        for(int j = 0; j < 25;  j++)
            i == j ? std::cout << "#" : std::cout << ".";
        std::cout<< std::endl;
    }   
}

void Spells::SpellOfThirdImage()
{
    const int MaxValueOfSecondLoop = 24;
    
    for(int i = 0; i < 25; i++)
    {
        for(int j = 0; j <= 24;  j++)
            MaxValueOfSecondLoop == j + i ? std::cout << "#" : std::cout << ".";
        std::cout<< std::endl;
    }   
}

int main(int argc, char* argv[])
{
    Spells* spells = new Spells;
    spells->PrintAllSpells();
    return 0;
}

